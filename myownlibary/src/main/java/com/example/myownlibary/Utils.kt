package com.example.myownlibary

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL


fun getBitmapFromDrawable(context: Context, sId: Int): Bitmap? {
    return BitmapFactory.decodeResource(
        context.resources,
        sId
    )
}

fun getBitmapFromNetwork(url: String?): Bitmap? {
    var urlValue: URL?
    try {
        urlValue = URL(url)
        return BitmapFactory.decodeStream(urlValue.openConnection().getInputStream())
    } catch (e: MalformedURLException) {
        e.printStackTrace()
        return null
    } catch (e: IOException) {
        e.printStackTrace()
        return null
    }
}
