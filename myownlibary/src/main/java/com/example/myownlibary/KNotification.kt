package com.example.myownlibary

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat


object KNotification {

    private var mContext: Context? = null
    private var CHANNEL_ID: String = "101"
    private var NOTIFY_ID: Int = 1001

    fun createChannel(context: Context, channelId: String, notifyId: Int): KNotification {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        CHANNEL_ID = channelId
        NOTIFY_ID = notifyId
        mContext = context
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Default"
            val descriptionText = "Example description"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelId, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }

        return this
    }


    fun createNotification(
        title: String,
        textContent: String,
        bigText: String? = null,
        smallIcon: Int,
        bigImage: Bitmap?,
        pendingIntent: PendingIntent
    ) {

//            val intent = Intent(this, MainActivity::class.java).apply {
//                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//            }


//            val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        mContext?.let { context ->
            var builder = NotificationCompat.Builder(context,
                CHANNEL_ID
            )
                .setSmallIcon(smallIcon)
                .setContentTitle(title)
                .setContentText(textContent)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            bigText?.let {
                builder.setStyle(
                    NotificationCompat.BigTextStyle()
                        .bigText(it)
                )
            }
            bigImage?.let {
                builder.setLargeIcon(it).setStyle(
                    NotificationCompat.BigPictureStyle()
                        .bigPicture(bigImage)
                        .bigLargeIcon(null)
                )
            }
            with(NotificationManagerCompat.from(context)) {
                // notificationId is a unique int for each notification that you must define
                notify(NOTIFY_ID, builder.build())
            }
        }
    }
}
