package com.example.knotification

import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myownlibary.KNotification
import com.example.myownlibary.getBitmapFromDrawable
import kotlinx.coroutines.*


class MainActivity : AppCompatActivity() {
    val CHANNEL_ID = "101"

    private val coroutineScope = CoroutineScope(Dispatchers.Main + Job())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val intent = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
            KNotification.createChannel(applicationContext, "100", 10001)
                .createNotification(
                    "Hello",
                    "I'm Kha",
                    smallIcon = R.drawable.ic_launcher_foreground,
                    bigImage = getBitmapFromDrawable(
                        this, R.drawable.noti
                    ), pendingIntent = pendingIntent
                )


    }
}
